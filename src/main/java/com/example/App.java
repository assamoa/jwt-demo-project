package com.example;

import com.example.app.GoogleJwtClient;
import java.net.URL;
/**
 * Hello world!
 */
public final class App {
    private App() {
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
        final String saKeyfile = "apigwtest-332904-a793f97ff48a.json";
        final String saEmail = "apigw-test-service@apigwtest-332904.iam.gserviceaccount.com";
        final String audience = "https://sample-gateway-cr6z9uu1.an.gateway.dev";
        final int expiryLength = 31536000;
        try{
            String jwt = GoogleJwtClient.generateJwt(saKeyfile, saEmail, audience, expiryLength);
            System.out.println("jwt="+jwt);
            URL url = new URL("https://sample-gateway-cr6z9uu1.an.gateway.dev/hello-auth");
            String result = GoogleJwtClient.makeJwtRequest(jwt, url);
            System.out.println(result);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
